﻿Imports System.Data.OleDb
Public Class Main
    Dim dbPath As String = Application.StartupPath & "\ProjSil.accdb"
    Dim connectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; DATA SOURCE = " & dbPath
    Public Function performQuery(ByVal connectionString As String, ByVal sqlCommand As String) As OleDb.OleDbDataReader
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbConnection.Open()
            dbDataReader = dbCommand.ExecuteReader
            Return dbDataReader
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        Return Nothing
    End Function
    Public Function performNonQuery(ByVal connectionString As String, ByVal sqlCommand As String) As Boolean
        Dim dbConnection As OleDbConnection
        Dim dbCommand As New OleDbCommand()

        Try
            dbConnection = New OleDbConnection(connectionString)
            dbCommand.CommandText = sqlCommand
            dbCommand.Connection = dbConnection
            dbCommand.Connection.Open()
            dbCommand.ExecuteNonQuery()
            dbConnection.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End Try

        Return True
    End Function
    Public Function disableFields()
        txtbContact.Enabled = False
        txtbEadd.Enabled = False
        txtbHadd.Enabled = False
        txtbName.Enabled = False
        txtbDate.Enabled = False
        cmbYear.Enabled = False
        cmbMonth.Enabled = False
        cmbType.Enabled = False
        cmbDay.Enabled = False
        Return Nothing

    End Function
    Public Function clearFields()
        txtbContact.Clear()
        txtbEadd.Clear()
        txtbDate.Clear()
        txtbName.Clear()
        txtbSearch.Clear()
        txtbHadd.Clear()
        txtbDate.Clear()
        cmbType.ResetText()
        cmbMonth.ResetText()
        cmbDay.ResetText()
        cmbYear.ResetText()

        Return Nothing
    End Function

    Public Function enableFields()
        txtbContact.Enabled = True
        txtbEadd.Enabled = True
        txtbHadd.Enabled = True
        txtbName.Enabled = True
        txtbDate.Enabled = True
        cmbYear.Enabled = True
        cmbMonth.Enabled = True
        cmbType.Enabled = True
        cmbDay.Enabled = True
        Return Nothing
    End Function


    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label3.Click

    End Sub

    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label5.Click

    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub Label7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label7.Click

    End Sub

    Private Sub Label8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label8.Click


    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDay.SelectedIndexChanged

    End Sub

    Private Sub Label9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label9.Click

    End Sub

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        disableFields()



    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtbName.Text.Trim.Length = 0 Then
            MessageBox.Show("ENTER A VALID NAME.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        If txtbContact.Text.Trim.Length = 0 Then
            MessageBox.Show("ENTER A VALID NUMBER.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        If txtbHadd.Text.Trim.Length = 0 Then
            MessageBox.Show("ENTER A VALID ADDRESS.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If txtbDate.Text.Trim.Length = 0 Then
            MessageBox.Show("ENTER A VALID EMAIL.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        Dim dbDataReader As OleDb.OleDbDataReader = Nothing
        Dim sqlCommand As String = "SELECT * FROM ProjSil WHERE NAME_S = '" & txtbName.Text & "'"
        dbDataReader = Me.performQuery(connectionString, sqlCommand)

        If dbDataReader.HasRows = 0 Then
            sqlCommand = "SELECT * FROM ProjSil WHERE CONTACT = '" & txtbContact.Text & "'"
            dbDataReader = Me.performQuery(connectionString, sqlCommand)
            If dbDataReader.HasRows = 0 Then

                sqlCommand = "INSERT INTO ProjSil (NAME_S,CONTACT,DATE_F,EMAILADD,HOMEADD,TYPE_S,MONTH_S,DATE_S,YEAR_S) VALUES('" & txtbName.Text & "', '" & txtbContact.Text & "','" & txtbDate.Text & "','" & txtbEadd.Text & "','" & txtbHadd.Text & "', '" & cmbType.Text & "','" & cmbMonth.Text & "','" & cmbDay.Text & "','" & cmbYear.Text & "')"
                If Me.performNonQuery(connectionString, sqlCommand) Then
                    MessageBox.Show("NEW FILE STORED.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    clearFields()
                    Exit Sub
                Else
                    MessageBox.Show("UNABLE TO SAVE NEW FILE.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    txtbName.Focus()
                    Exit Sub
                End If
            Else
                MessageBox.Show("NUMBER ALREADY EXIST", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtbContact.Focus()
                Exit Sub
            End If
        Else
            MessageBox.Show("NAME ALREADY EXIST.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtbName.Focus()
            Exit Sub
        End If
    End Sub


    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        btnCancel.Hide()
        btnBack.Show()

        Dim dbDataReader As OleDb.OleDbDataReader = Nothing
        Dim sqlCommand1 As String = "SELECT * FROM ProjSil WHERE NAME_S = '" & txtbSearch.Text & "'"

        dbDataReader = Me.performQuery(connectionString, sqlCommand1)

        If dbDataReader.HasRows Then



            While dbDataReader.Read
                Me.txtbName.Text = dbDataReader("NAME_S".ToString)
                Me.txtbContact.Text = dbDataReader("CONTACT".ToString)
                Me.txtbHadd.Text = dbDataReader("HOMEADD".ToString)
                Me.txtbDate.Text = dbDataReader("EMAILADD".ToString)
                Me.cmbType.Text = dbDataReader("TYPE_S".ToString)
                Me.cmbYear.Text = dbDataReader("YEAR_S".ToString)
                Me.cmbMonth.Text = dbDataReader("MONTH_S".ToString)
                Me.cmbDay.Text = dbDataReader("DATE_S".ToString)
                Me.txtbEadd.Text = dbDataReader("DATE_F".ToString)

            End While
            disableFields()
        Else
            MessageBox.Show("FILE NOT FOUND.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)

            Exit Sub
        End If
    End Sub

    Public Sub txtbName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbName.TextChanged

    End Sub

    Public Sub txtbContact_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbContact.TextChanged

    End Sub

    Public Sub txtbHadd_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbHadd.TextChanged

    End Sub

    Public Sub txtbEadd_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbDate.TextChanged

    End Sub

    Public Sub cmbType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbType.SelectedIndexChanged

    End Sub

    Public Sub txtbDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtbEadd.TextChanged

    End Sub

    Public Sub cmbMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMonth.SelectedIndexChanged

    End Sub

    Public Sub cmbDay_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbYear.SelectedIndexChanged

    End Sub

    Public Sub btnAddPhoto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddPhoto.Click

    End Sub

    Public Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        OptionP.Show()
        Me.Hide()
        clearFields()


    End Sub
End Class